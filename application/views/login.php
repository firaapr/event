<!DOCTYPE html>
<html lang="en">

<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- site metas -->
    <title>Event</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="<?php echo base_url("assets/css/login.css") ?>">
	
	<style>
		body{
			height: 100%;
			background: url("assets/images/banner.jpg")no-repeat center center fixed;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
			backdrop-filter: blur(1px);
		}
		.rectangle{
			background: url("assets/images/header-bg.jpg");
			border: solid 10px;
			text-align: center;
			width: 500px;
			margin-left: auto;
			margin-right: auto;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
			min-height: 550px;
			max-height: 550px;
			margin-top: 30px;
		}
		.side{
			background-color: darkgray !important;
		}
		.white{
			color: white !important;
			text-align: left;
		}
		.form{
			padding: 20px;
		}
	</style>
</head>

<body>
    <div class="main">
		<div class="rectangle">
			<div class="logo" style="margin-top: 20px"><a href="index.html"><img src="<?php echo base_url("assets/images/logo.png") ?>" width="80px" alt="#"></a></div>
			<form class="form" action="<?php echo base_url('index.php/login/doLogin'); ?>" method="post">
				<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Sign In</label>
				<div class="login-form">
					<div class="sign-in-htm">
						<br><br>
						<div class="group">
                            <label for="user" class="label white">Email</label>
                            <input id="user" type="email" name="email" class="input side" data-type="user">
                        </div>
                        <div class="group">
                            <label for="pass" class="label white">Password</label>
                            <input id="pass" type="password" name="password" class="input side" data-type="password">
                        </div>
                        <br><br>
                        <div class="group">
                            <input type="submit" class="button" value="Sign In">
                            <div class="row" style="margin-left: -40px"></div>
                            <div class="hr"></div>
                        </div>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>

</html>