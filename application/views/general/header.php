<!DOCTYPE html>
<html lang="en">

<head>
   <!-- basic -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <!-- mobile metas -->
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="viewport" content="initial-scale=1, maximum-scale=1">
   <!-- site metas -->
   <title>Event Stiki</title>
   <meta name="keywords" content="">
   <meta name="description" content="">
   <meta name="author" content="">
   <!-- bootstrap css -->
   <link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.min.css") ?>">
   <!-- style css -->
   <link rel="stylesheet" href="<?php echo base_url("assets/css/style.css") ?>">
   <!-- Responsive-->
   <link rel="stylesheet" href="<?php echo base_url("assets/css/responsive.css") ?>">
   <!-- fevicon -->
   <!-- <link rel="icon" href="<?php echo base_url("assets/images/fevicon.png") ?>" type="image/gif" /> -->
   <!-- Scrollbar Custom CSS -->
   <link rel="stylesheet" href="<?php echo base_url("assets/css/jquery.mCustomScrollbar.min.css") ?>">
   <!-- Tweaks for older IEs-->
   <!-- owl stylesheets -->
   <!-- <link rel="stylesheet" href="<?php echo base_url("assets/css/owl.carousel.min.css") ?>"> -->
   <!-- <link rel="stylesheet" href="<?php echo base_url("assets/css/owl.theme.default.min.css") ?>"> -->
   <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<!-- body -->

<body class="main-layout">
   <!-- loader  -->
   <!-- <div class="loader_bg">
         <div class="loader"><img src="<?php echo base_url("assets/images/loading.gif") ?>" alt="#" /></div>
      </div> -->
   <!-- end loader -->
   <!-- header -->
   <header>
      <!-- header inner -->
      <div class="header">
         <div class="header_white_section">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-12">
                     <div class="header_information">
                        <ul>
                           <li><img src="<?php echo base_url("assets/images/hello.png") ?>" width="40px" alt="#" /> Halo , <?php echo $this->session->userdata("name"); ?> silahkan ajukan proposal kegiatanmu </li>
                        </ul>

                        <div class="pull-right close" style="margin-right:5%">
                           <a href="<?php echo site_url('logout/doLogout'); ?>">
                              <img src="<?php echo base_url("assets/images/logout.png") ?>" width="35px">
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="row">
               <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col logo_section">
                  <div class="full">
                     <div class="center-desk">
                        <div class="logo"> <a href="index.html"><img src="<?php echo base_url("assets/images/logo4.png") ?>" width="200px" alt="#"></a></div>
                     </div>
                  </div>
               </div>
               <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                  <div class="menu-area">
                     <div class="limit-box">
                        <nav class="main-menu">
                           <ul class="menu-area-main">
                              <?php if ($this->session->userdata("role") == 'mahasiswa') { ?>
                                 <li> <a href="<?php echo base_url('index.php/main'); ?>">Home</a></li>
                              <?php } ?>
                              <li> <a href="<?php echo base_url('index.php/main/listProposal'); ?>">Proposal Saya</a></li>
                           </ul>
                        </nav>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end header inner -->
   </header>
   <!-- end header -->