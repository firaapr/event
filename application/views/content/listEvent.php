<section>
    <div class="banner-main">
        <img src="<?php echo base_url("assets/images/banner.jpg") ?>" alt="#" />
        <div class="container">
            <div class="text-bg" style="top: 30% !important">
                <br><br>
                <div class="container">
                    <form class="main-form">
                        <?php if ($this->session->userdata("role") == 'mahasiswa') { ?>
                            <h3>Proposal Saya</h3>
                        <?php } else { ?>
                            <h3>List Pengajuan Proposal</h3>
                        <?php }  ?>
                        <div class="row">
                            <table class="table" id="tblEvent" width="1070px">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>No Proposal</th>
                                        <th>Nama Event</th>
                                        <th>Tanggal Dibuat</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (isset($data) != false && empty($data) == false) {
                                        $i = 0;
                                        foreach ($data as $data[0][]) { ?>
                                            <tr>
                                                <th scope="row"><?= $data[$i]['id_proposal'] ?></th>
                                                <td><?= $data[$i]['event_name'] ?></td>
                                                <td><?= $data[$i]['created_at'] ?></td>
                                                <td><?= $data[$i]['status'] ?>
                                                <td>
                                                    <?php if ($this->session->userdata("role") == 'mahasiswa') { ?>
                                                        <button type="button" class="btn btn-primary" onclick="location.href='<?= base_url() . 'index.php/main/editProposal/' . $data[$i]['id_proposal'] ?>';">
                                                            Detail</button>
                                                    <?php } else { ?>
                                                        <button type="button" class="btn btn-primary" onclick="location.href='<?= base_url() . 'index.php/approval/detail/' . $data[$i]['id_proposal'] ?>';">
                                                            Aksi</button>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php $i++;
                                        }
                                    } else { ?>
                                        <tr>
                                            <td colspan="5">Tidak ada data</td>
                                        </tr>
                                    <?php } ?>
                                    <!-- <tr>
                                        <th scope="row">P200000001</th>
                                        <td>Kompetisi Ilmiah</td>
                                        <td>04/02/2019</td>
                                        <td>Approved - Final</td>
                                        <td><button type="button" class="btn btn-primary" onclick="location.href='<?= base_url() . 'index.php/approval' ?>';">Detail</button></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">P200000002</th>
                                        <td>Kompetisi Kesenian</td>
                                        <td>07/09/2019</td>
                                        <td>Approved - Final</td>
                                        <td><button type="button" class="btn btn-primary">Detail</button></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">P200000003</th>
                                        <td>Kompetisi Bahasa</td>
                                        <td>03/01/2020</td>
                                        <td>Approved - Final</td>
                                        <td><button type="button" class="btn btn-primary">Detail</button></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">P200000005</th>
                                        <td>Kompetisi Olahraga</td>
                                        <td>19/05/2020</td>
                                        <td>Approved - Final</td>
                                        <td><button type="button" class="btn btn-primary">Detail</button></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">P200000006</th>
                                        <td>Lomba 17-an</td>
                                        <td>01/12/2020</td>
                                        <td>InProgress - Pembina</td>
                                        <td><button type="button" class="btn btn-primary">Detail</button></td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>