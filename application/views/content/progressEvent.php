<style>
	.icon{
		width: 60px;
		margin-bottom: 20px;
		margin-top: 7px
	}
	.ul{
		display: inline-block;
		font-family: sans-serif;
		position: relative;
	}
	.li{
		padding: 25px;
		display: inline-block;
		font-family: sans-serif;
		position: relative;
	}
	.white{
		color: #0a4668;
		font-weight: bold;
	}
	.flow{
		margin-top: -300px;
	}
	.fa{
		background: #0a4668;
		width: 20px;
		height: 20px;
		color: #fff;
		border-radius: 50%;
		padding: 3px;
	}
	.fa::after{
		content: '';
		background: #0a4668;
		height: 5px;
		width: 210px;
		display: block;
		position: absolute;
		left: 0;
		top: 120px;
		z-index: -1;
	}
	.special{
		margin-top: -310px;
		background-color: #ffffffe8;
		width: 82%;
		position: fixed;
		left: 9%;
	}
</style>

<section>
    <div class="banner-main">
        <img src="<?php echo base_url("assets/images/banner.jpg") ?>" alt="#" />
        <div class="container">
            <div class="text-bg">
                <br><br>
                <div class="special">
					<ul class="ul">
						<li class="li">
							<img class="icon" src="<?php echo base_url("assets/images/step-2.png") ?>" alt="#"><br>
							<div class="fa">1</div>
							<p class="white"><?= $proposal['progress'][0]['status'] ?> - <?= $proposal['progress'][0]['step_user'] ?></p>
						</li>
						<li class="li">
							<img class="icon" src="<?php echo base_url("assets/images/step-3.png") ?>" alt="#"><br>
							<div class="fa">2</div>
							<p class="white"><?= $proposal['progress'][1]['status'] ?> - <?= $proposal['progress'][1]['step_user'] ?></p>
						</li>
						<li class="li">
							<img class="icon" src="<?php echo base_url("assets/images/step-2.png") ?>" alt="#"><br>
							<div class="fa">3</div>
							<p class="white"><?= $proposal['progress'][2]['status'] ?> - <?= $proposal['progress'][2]['step_user'] ?></p>
						</li>
						<li class="li">
							<img class="icon" src="<?php echo base_url("assets/images/step-3.png") ?>" alt="#"><br>
							<div class="fa">4</div>
							<p class="white"><?= $proposal['progress'][3]['status'] ?> - <?= $proposal['progress'][3]['step_user'] ?></p>
						</li>
						<li class="li">
							<img class="icon" src="<?php echo base_url("assets/images/step-2.png") ?>" alt="#"><br>
							<div class="fa">5</div>
							<p class="white"><?= $proposal['progress'][4]['status'] ?> - <?= $proposal['progress'][4]['step_user'] ?></p>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="container">
				<div class="text-bg" style="top: 110% !important">
					<br><br>
					<div class="container">
						<form id="myform" class="main-form" action="<?php echo base_url('index.php/main/actionEdit/' . $proposal['id_proposal']); ?>" method="post" enctype='multipart/form-data'>
							<h3>Ubah Proposal</h3>
							<?php
							$start = date('yy-m-d\TH:i', strtotime($proposal['start_event']));
							$end = date('yy-m-d\TH:i', strtotime($proposal['end_event']));

							foreach ($proposal['progress'] as $value) {
								
								if ($value['status'] == 'rejected') {
									$idApproval = $value['id_approval'];
									$status = $value['status'];
								} else {
									$idApproval = '';
									$status = '';
								}
							}
							?>
							<div class="row">
								<input class="form-control" placeholder="" type="hidden" name="id_approval" value="<?= $idApproval ?>">
								<div class="col-md-12">
									<div class="row">
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
											<label>NRP</label>
											<input class="form-control" placeholder="" type="text" name="nrp" value="<?= $proposal['id_user'] ?>" disabled>
										</div>
										<div class="col-xl-8 col-lg-4 col-md-4 col-sm-6 col-12">
											<label>Nama Lengkap</label>
											<input class="form-control" placeholder="" type="text" name="name" value="<?= $proposal['name_submitted'] ?>" disabled>
										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
											<label>Bank</label>
											<select class="form-control" name="bank_name">
												<option>Pilih</option>
												<option value="BNI" <?php if ($proposal['bank_name'] == 'BNI') echo "selected"; ?>>BNI</option>
												<option value="BRI" <?php if ($proposal['bank_name'] == 'BRI') echo "selected"; ?>>BRI</option>
												<option value="BCA" <?php if ($proposal['bank_name'] == 'BCA') echo "selected"; ?>>BCA</option>
												<option value="MANDIRI" <?php if ($proposal['bank_name'] == 'MANDIRI') echo "selected"; ?>>MANDIRI</option>
												<option value="CIMB NIAGA" <?php if ($proposal['bank_name'] == 'CIMB NIAGA') echo "selected"; ?>>CIMB NIAGA</option>
											</select>
										</div>
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
											<label>Nomer Rekening</label>
											<input class="form-control" placeholder="" type="text" name="rekening" value="<?= $proposal['bank_account_no'] ?>">
										</div>
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
											<label>Nama Akun Bank</label>
											<input class="form-control" placeholder="" type="text" name="bank_account_name" value="<?= $proposal['bank_account_name'] ?>">
										</div>
									</div>
								</div>
							</div>
							</br>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
											<label>Program Studi</label>
											<select class="form-control" name="study_program" disabled>
												<option>Pilih</option>
												<option value="TI" <?php if ($proposal['study_program'] == 'TI') echo "selected"; ?>>Teknik Informatika</option>
												<option value="DKV" <?php if ($proposal['study_program'] == 'DKV') echo "selected"; ?>>Desain Komunikasi Visual</option>
												<option value="SI" <?php if ($proposal['study_program'] == 'SI') echo "selected"; ?>>Sistem Informasi</option>
												<option value="MI" <?php if ($proposal['study_program'] == 'MI') echo "selected"; ?>>Manajemen Informatika</option>
											</select>
										</div>
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
											<label>Organisasi</label>
											<input class="form-control" placeholder="" type="text" name="organization" value="<?= $proposal['organization'] ?>" disabled>
										</div>
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
											<label>Posisi</label>
											<input class="form-control" placeholder="" type="text" name="position" value="<?= $proposal['position'] ?>" disabled>
										</div>
									</div>
								</div>
							</div>
							</br>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
											<label>Tipe Kegiatan</label>
											<select class="form-control" name="event_type" disabled>
												<option>Pilih</option>
												<option value="kesenian" <?php if ($proposal['event_type'] == 'kesenian') echo "selected"; ?>>Kesenian</option>
												<option value="kompetisi" <?php if ($proposal['event_type'] == 'kompetisi') echo "selected"; ?>>Kompetisi</option>
												<option value="seminar" <?php if ($proposal['event_type'] == 'seminar') echo "selected"; ?>>Seminar</option>
												<option value="keagamaan" <?php if ($proposal['event_type'] == 'keagamaan') echo "selected"; ?>>Keagamaan</option>
												<option value="sosial" <?php if ($proposal['event_type'] == 'sosial') echo "selected"; ?>>Sosial</option>
											</select>
										</div>
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
											<label>Nama Kegiatan</label>
											<input class="form-control" placeholder="" type="text" name="event_name" value="<?= $proposal['event_type'] ?>" disabled>
										</div>
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
											<label>Lokasi</label>
											<input class="form-control" placeholder="" type="text" name="place" value="<?= $proposal['place'] ?>">
										</div>
									</div>
								</div>
							</div>
							</br>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
											<label>Mulai Kegiatan</label>
											<input class="form-control" placeholder="Any" type="datetime-local" name="start_event" value="<?= $start ?>">
										</div>
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
											<label>Akhir Kegiatan</label>
											<input class="form-control" placeholder="Any" type="datetime-local" name="end_event" value="<?= $end ?>">
										</div>
										<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
											<label>Budget</label>
											<input class="form-control" placeholder="" type="text" name="budget" value="<?= $proposal['budget'] ?>">
										</div>
									</div>
								</div>
							</div>
							</br>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-xl-12 col-lg-4 col-md-4 col-sm-6 col-12">
											<label>File Proposal</label>
											<input class="form-control" placeholder="" type="file" name="proposal_file" value="<?= $proposal['file_proposal'] ?>">
										</div>
									</div>
								</div>
							</div>
							<div class="row" style="margin-left: -40px">
								<div class="col-xl-6 col-lg-3 col-md-3 col-sm-12 col-12">
									<!-- <a href="#">Submit Ulang</a> -->
									<a href="javascript:void()" onclick="document.getElementById('myform').submit();">Submit Ulang</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
        </div>
    </div>
</section>