<section>
    <div class="banner-main">
        <img src="<?php echo base_url("assets/images/banner.jpg") ?>" alt="#" />
        <div class="container">
            <div class="text-bg" style="margin-top:10%">
                <br><br>
                <div class="container">
                    <form id="myform" class="main-form" action="<?php echo base_url('index.php/approval/action_edit'); ?>" method="post">
                        <h3>Form Persetujuan</h3>
                        <div class="row" style="margin-left: 1%;">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>NRP</label>
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label style="color: black;font-weight: bold;"><?= $data[0]['id_user'] ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Nama Lengkap</label>
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label style="color: black;font-weight: bold;"><?= $data[0]['name_submitted'] ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Jurusan</label>
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label style="color: black;font-weight: bold;"><?= $data[0]['study_program'] ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Posisi dalam Organisasi</label>
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label style="color: black;font-weight: bold;"><?= $data[0]['position'] ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Lihat Proposal</label>
                                    </div>
                                    <div class="col-xl-1 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <button type="button" class="btn btn-primary" onclick="location.href='<?= base_url() . 'index.php/approval/showFile/' . $data[0]['file_proposal'] ?>';">Lihat</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br><br><br>
                        <?php if ($data[0]['status'] != 'approved') { ?>
                            <div>
                                <div style="margin-left: 1%;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-xl-10 col-lg-4 col-md-4 col-sm-6 col-12">
                                                <label>Status</label>
                                                <select class="form-control" name="status">
                                                    <option value="">Pilih satu</option>
                                                    <option value="approved">Setuju</option>
                                                    <option value="rejected">Tolak</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-xl-10 col-lg-4 col-md-4 col-sm-6 col-12">
                                                <label>Keterangan (*)</label>
                                                <input class="form-control" placeholder="" type="text" name="note">
                                            </div>
                                        </div>
                                    </div>
                                    <input class="form-control" placeholder="" type="hidden" name="id_proposal" value="<?= $data[0]['id_proposal'] ?>">
                                    <input class="form-control" placeholder="" type="hidden" name="id_approval" value="<?= $data[0]['id_approval'] ?>">
                                    <input class="form-control" placeholder="" type="hidden" name="step" value="<?= $data[0]['step'] ?>">
                                </div>
                                <div class="row">
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                        <a href="javascript:void()" onclick="document.getElementById('myform').submit();">Simpan</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</section>