<section>
    <div class="banner-main">
        <img src="<?php echo base_url("assets/images/banner.jpg") ?>" alt="#" />
        <div class="container" >
            <div class="text-bg" style="margin-top:10%">
                <div class="container">
                    <form class="main-form" action="<?php echo base_url('index.php/main/submitProposal'); ?>" method="post" enctype='multipart/form-data'>
                        <h3>Form Perizinan</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>NRP</label>
                                        <input class="form-control" placeholder="Masukkan NRP" type="text" name="txtNRP">
                                    </div>
                                    <div class="col-xl-8 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Nama Lengkap</label>
                                        <input class="form-control" placeholder="Masukkan Nama Lengkap" type="text" name="txtName">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Bank</label>
                                        <select class="form-control" name="cbBank">
                                            <option value="">Pilih Bank</option>
                                            <option value="BNI">BNI</option>
                                            <option value="BRI">BRI</option>
                                            <option value="BCA">BCA</option>
                                            <option value="MANDIRI">MANDIRI</option>
                                            <option value="CIMB NIAGA">CIMB NIAGA</option>
                                        </select>
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Nomer Rekening</label>
                                        <input class="form-control" placeholder="Masukkan Nomor Rekening" type="text" name="txtBankNo">
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Nama Akun Bank</label>
                                        <input class="form-control" placeholder="Masukkan Nama Akun Bank" type="text" name="txtBankAcc">
                                    </div>                              
                                </div>
                            </div>
                        </div>
						</br>
						<div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Program Studi</label>
                                        <select class="form-control" name="cbProdi">
                                            <option value="">Pilih Program Studi</option>
                                            <option value="TI">Teknik Informatika</option>
                                            <option value="DKV">Desain Komunikasi Visual</option>
                                            <option value="SI">Sistem Informasi</option>
                                            <option value="MI">Manajemen Informatika</option>
                                        </select>
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Organisasi</label>
                                        <input class="form-control" placeholder="Masukkan Organisasi" type="text" name="txtOrg">
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Posisi Organisasi</label>
                                        <input class="form-control" placeholder="Masukkan Posisi" type="text" name="txtPos">
                                    </div>
                                </div>
                            </div>
                        </div>
						</br>
						<div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Tipe Kegiatan</label>
                                        <select class="form-control" name="cbType">
                                            <option value="">Pilih Tipe</option>
                                            <option value="kesenian">Kesenian</option>
                                            <option value="kompetisi">Kompetisi</option>
                                            <option value="seminar">Seminar</option>
                                            <option value="keagamaan">Keagamaan</option>
                                            <option value="sosial">Sosial</option>
                                        </select>
                                    </div>
									<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Nama Kegiatan</label>
                                        <input class="form-control" placeholder="Masukkan Nama Kegiatan" type="text" name="txtEvent">
                                    </div>
									<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Lokasi</label>
                                        <input class="form-control" placeholder="Masukkan Lokasi" type="text" name="txtLocation">
                                    </div>
                                </div>
                            </div>
                        </div>
						</br>
						<div class="row">
                            <div class="col-md-12">
                                <div class="row">
									<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Mulai Kegiatan</label>
                                        <input class="form-control" placeholder="" type="datetime-local" name="txtStart">
                                    </div>
									<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Akhir Kegiatan</label>
                                        <input class="form-control" placeholder="" type="datetime-local" name="txtEnd">
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>Budget</label>
                                        <input class="form-control" placeholder="Masukkan Budget" type="text" name="txtBudget">
                                    </div>
                                </div>
                            </div>
                        </div>
						</br>
						<div class="row">
							<div class="col-md-12">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-4 col-md-4 col-sm-6 col-12">
                                        <label>File Proposal</label>
                                        <input class="form-control" placeholder="" type="file" name="proposal_file">
                                    </div>
                                </div>
                            </div>
						</div>
                        <div class="row" style="margin-left: -100px;margin-top: 30px">
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                <!-- <a href="#">Ajukan</a> -->
                                <!-- <input class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12" type="submit" name="submit" value="Submit"> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>