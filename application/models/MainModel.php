<?php

class MainModel extends CI_Model
{
  public function show_all()
  {
    $data = $this->db->get('barang');
    return $data->result_array();
  }

  function input_data($table, $data)
  {
    if($this->db->insert($table, $data)){
      return true;
    }
    else{
      return false;
    }
  }

  function showData_byId($id, $field, $table)
  {
    $data = $this->db->get_where($table, array($field => $id));
    return $data->result_array();
  }

  function listProposal($id, $role)
  {
    if ($role == "mahasiswa") {
      $data = "select * from proposal join 
                  (select * from approval where id_approval in 
                    (select id_approval from approval ap where step = 
                      (select max(step) from approval a where status <> 'pending' AND a.id_proposal = ap.id_proposal) 
                  )) 
                  as app on proposal.id_proposal = app.id_proposal where proposal.id_user=" . $id;
      $query = $this->db->query($data);
      return $query->result_array();
    } else {
      $data = "select * from approval a JOIN proposal p 
                ON a.id_proposal = p.id_proposal 
                WHERE a.status <> 'pending' AND a.user_approval ='" . $id . "'";
      $query = $this->db->query($data);
      return $query->result_array();
    }
  }

  function lastCode()
  {
    $query = "select id_proposal from proposal order by created_at desc LIMIT 1";
    $data = $this->db->query($query);
    return $data->result_array()[0];
  }

  function detailProposal($id_proposal){
    $query = "select p.*,u.no_id,u.name as name_user from proposal p join user u on p.id_user = u.no_id where p.id_proposal='" .$id_proposal ."'";
    $data = $this->db->query($query);
    return $data->result_array()[0];
  }

  function viewProgress($id_proposal){
    $query = "select a.step,u.role as step_user,a.status,a.id_approval from approval a join user u on a.user_approval = u.no_id where a.id_proposal='" .$id_proposal."'";
    $data = $this->db->query($query);
    return $data->result_array();
  }

  function update_data($where, $data, $table)
  {
    $this->db->where($where);
    $this->db->update($table, $data);
  }
}
