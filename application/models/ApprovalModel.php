<?php

class ApprovalModel extends CI_Model
{
  function show_detail($id, $id_user, $role)
  {
    if ($role == "mahasiswa") {
      $data = "select * from approval a 
      JOIN proposal p 
      ON a.id_proposal = p.id_proposal 
      WHERE a.id_proposal ='" . $id .
        "' group by a.id_proposal";

      $query = $this->db->query($data);
      return $query->result_array();
    } else {
      $data = "select * from approval a 
      JOIN proposal p 
      ON a.id_proposal = p.id_proposal 
      WHERE a.id_proposal ='" . $id .
        "' and a.user_approval = '" . $id_user . "'";

      $query = $this->db->query($data);
      return $query->result_array();
    }
  }

  function update_data($where, $data, $table)
  {
    $this->db->where($where);
    $this->db->update($table, $data);
  }
}
