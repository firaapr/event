<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('MainModel');
        $this->load->helper('string');
    }

    public function index()
    {
        // $this->load->view('welcome_message');
        if ($this->session->userdata("role") == 'mahasiswa') {
            $data['content'] = "main";
            $this->load->view('main', $data);
        } else {
            $this->listProposal();
        }
    }

    public function listProposal()
    {
        $id_user = $this->session->userdata("id");
        $role = $this->session->userdata("role");

        $data['data'] = $this->MainModel->listProposal($id_user, $role);

        $data['content'] = "listEvent";
        $this->load->view('main', $data);
        // return $data;
    }

    public function submitProposal()
    {
        try {
            $nrp = $this->input->post('txtNRP'); //nrp;
            $name = $this->input->post('txtName'); //nama;
            $bankAccName = $this->input->post('txtBankAcc'); //namaaccbank;
            $bank = $this->input->post('cbBank'); //bank;
            $backAccNo = $this->input->post('txtBankNo'); //no rek;
            $studyProgram = $this->input->post('cbProdi'); //prodi;
            $organization = $this->input->post('txtOrg'); //organisasi;
            $orPosition = $this->input->post('txtPos'); //posisi organisasi;
            $eventType = $this->input->post('cbType'); //tipe;
            $eventName = $this->input->post('txtEvent'); //email;
            $eventPlace = $this->input->post('txtLocation'); //email;
            $eventStart = $this->input->post('txtStart'); //email;
            $eventEnd = $this->input->post('txtEnd'); //email;
            $eventBudget = $this->input->post('txtBudget'); //email;

            //code
            $year = substr(date("Y"), -2);
            $proposalCode = "P" . $year . "00001";
            $lastCode = $this->MainModel->lastCode();
            if ($lastCode['id_proposal']) {
                $newOrder = ((int)substr($lastCode['id_proposal'], -5)) + 1;
                $year = substr(date("Y"), -2);
                $proposalCode = "P" . $year . str_pad($newOrder, 5, '0', STR_PAD_LEFT);
            }

            $config['upload_path'] = '././assets/proposal/';
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = 2000;

            $id_user = $this->session->userdata("id");
            $newFilename = $id_user . "_" . $proposalCode;
            $config['file_name'] = $newFilename;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('proposal_file')) {
                $error = array('error' => $this->upload->display_errors());
                return [
                    "status" => false,
                    "message" => $error
                ];
            }

            $data = array(
                'id_proposal' => $proposalCode,
                'id_user' => $id_user,
                'name_submitted' => $name,
                'identity_no' => $nrp,
                'bank_name' => $bank,
                'bank_account_name' => $bankAccName,
                'bank_account_no' => $backAccNo,
                'study_program' => $studyProgram,
                'organization' => $organization,
                'position' => $orPosition,
                'event_type' => $eventType,
                'event_name' => $eventName,
                'start_event' => $eventStart,
                'end_event' => $eventEnd,
                'place' => $eventPlace,
                'budget' => $eventBudget,
                'file_proposal' => $newFilename . ".pdf"
            );

            if ($this->MainModel->input_data("proposal", $data)) {
                $userApproval[0] = "D123456";
                $userApproval[1] = "D789212";
                $userApproval[2] = "D986543";
                $userApproval[3] = "D654678";
                $userApproval[4] = "D111234";

                for ($i = 0; $i < 5; $i++) {
                    $status = "inProgress";
                    if ($i > 0) {
                        $status = "pending";
                    }
                    $dataApprove = array(
                        "id_proposal" => $proposalCode,
                        "step" => ($i + 1),
                        "user_approval" => $userApproval[$i],
                        "status" => $status,
                        "updated_at" => date('Y-m-d H:i:s')
                    );

                    $this->MainModel->input_data("approval", $dataApprove);
                }

                redirect(base_url('index.php/main/listProposal'));
            } else {
                return [
                    "status" => false,
                    "message" => "error"
                ];
            }
        } catch (\Exception $e) {
            return [
                "status" => false,
                "message" => $e->getMessage()
            ];
        }
    }

    public function detailProposal($id_proposal)
    {
        $id_user = $this->session->userdata("id");
        $role = $this->session->userdata("role");
        // $id_proposal = "P2000002";

        $data['proposal'] = $this->MainModel->detailProposal($id_proposal);

        if ($role == "mahasiswa") {
            $data['proposal']['progress'] = $this->MainModel->viewProgress($id_proposal);
        }

        return $data;
    }

    function editProposal($id_proposal)
    {
        $data = $this->detailProposal($id_proposal);
        $data['content'] = "progressEvent";
        $this->load->view('main', $data);
    }

    public function actionEdit($id_proposal)
    {
        try {
            $nrp = $this->input->post('nrp'); //nrp;
            $name = $this->input->post('name'); //nama;
            $bankAccName = $this->input->post('bank_account_name'); //namaaccbank;
            $bank = $this->input->post('bank_name'); //bank;
            $backAccNo = $this->input->post('rekening'); //no rek;
            $studyProgram = $this->input->post('study_program'); //prodi;
            $organization = $this->input->post('organization'); //organisasi;
            $orPosition = $this->input->post('position'); //posisi organisasi;
            $eventType = $this->input->post('event_type'); //tipe;
            $eventName = $this->input->post('event_name'); //email;
            $eventPlace = $this->input->post('place'); //email;
            $eventStart = $this->input->post('start_event'); //email;
            $eventEnd = $this->input->post('end_event'); //email;
            $eventBudget = $this->input->post('budget'); //email;
            $idApproval = $this->input->post('id_approval');

            $config['upload_path'] = '././assets/proposal/';
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = 2000;

            $id_user = $this->session->userdata("id");
            $newFilename = $id_user . "_" . $id_proposal . "_revisi";
            $config['file_name'] = $newFilename;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('proposal_file')) {
                $error = array('error' => $this->upload->display_errors());
                return [
                    "status" => false,
                    "message" => $error
                ];
            }

            $data = array(
                // 'id_user' => $id_user,
                // 'name_submitted' => $name,
                // 'identity_no' => $nrp,
                'bank_name' => $bank,
                'bank_account_name' => $bankAccName,
                'bank_account_no' => $backAccNo,
                // 'study_program' => $studyProgram,
                // 'organization' => $organization,
                // 'position' => $orPosition,
                // 'event_type' => $eventType,
                // 'event_name' => $eventName,
                'start_event' => $eventStart,
                'end_event' => $eventEnd,
                'place' => $eventPlace,
                'budget' => $eventBudget,
                'file_proposal' => $newFilename . ".pdf"
            );

            $where = array(
                'id_proposal' => $id_proposal
            );

            $this->MainModel->update_data($where, $data, 'proposal');
            $this->updateNextStep($idApproval);
        } catch (Exception $e) {
            return [
                "status" => false,
                "message" => $e->getMessage()
            ];
        }
    }

    function updateNextStep($idApproval)
    {
        $data = array(
            'status' => 'inProgress',
            'note' => '',
        );

        $where = array(
            'id_approval' => $idApproval
        );

        $this->MainModel->update_data($where, $data, 'approval');
        redirect(base_url('index.php/main/listProposal'));
    }
}
