<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
    {
        parent::__construct();
        $this->load->model('LoginModel');
    }

	public function index()
	{
		$this->load->view('login');
	}

	function doLogin()
	{
		// redirect(base_url('index.php/welcome'));
		$email = $this->input->post('email');
		$pass = $this->input->post('password');

		$cek = $this->LoginModel->get_data($email, $pass);
		if ($cek > 0) {
			$data_session = array(
				'name' => $cek[0]['name'],
				'status' => "login",
				'role' => $cek[0]['role'],
				'id' => $cek[0]['no_id'],
				'position' => $cek[0]['position'],
				'email' => $cek[0]['email'],
			);

			$this->session->set_userdata($data_session);
			redirect(base_url('index.php/main'));
		} else {
			echo "Username dan password salah !";
		}
	}
}
