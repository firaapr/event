<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Approval extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('ApprovalModel');
    }

    public function index()
    {
        $data['content'] = "approval";
        $this->load->view('main', $data);
    }

    public function detail($id)
    {
        $id_user = $this->session->userdata("id");
        $role = $this->session->userdata("role");

        $cek = $this->ApprovalModel->show_detail($id, $id_user, $role);
        if ($cek > 0) {
            $data['data'] = $cek;
        }

        $data['content'] = "approval";
        $this->load->view('main', $data);
    }

    public function showFile()
    {
        $filename = base_url('assets/proposal/yuhu.pdf');

        header("Content-Length: " . filesize($filename));
        header("Content-type: application/pdf");
        header("Content-disposition: inline; filename=" . basename($filename));
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        ob_clean();
        flush();
        readfile($filename);

        // $data['content'] = "showFile";
        // $this->load->view('main', $data);
    }

    function action_edit()
    {
        $id_user = $this->session->userdata("id");
        $id_approval = $this->input->post('id_approval');
        $id_proposal = $this->input->post('id_proposal');
        $step = $this->input->post('step');
        $status = $this->input->post('status');

        $data = array(
            'id_proposal' => $id_proposal,
            'step' => $step,
            'user_approval' => $id_user,
            'status' => $status,
            'note' => $this->input->post('note')
        );

        $where = array(
            'id_approval' => $id_approval
        );

        $this->ApprovalModel->update_data($where, $data, 'approval');

        if ($step < 5 && $status != 'rejected') {
            $this->update_next_step($id_proposal, $step);
        } else {
            redirect(base_url('index.php/main/listProposal'));
        }
    }

    function update_next_step($id_proposal, $step)
    {
        $data = array(
            'status' => 'inProgress'
        );

        $where = array(
            'id_proposal' => $id_proposal,
            'step' => $step + 1
        );

        $this->ApprovalModel->update_data($where, $data, 'approval');
        redirect(base_url('index.php/main/listProposal'));
    }
}
